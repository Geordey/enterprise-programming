﻿using DataAccess;
using EntAssignment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class BlogOperations
    {
        public List<blog> GetAllBlogs()
        {
            BlogRepo br = new BlogRepo();
            return br.GetBlogs();
        }
    }
}
