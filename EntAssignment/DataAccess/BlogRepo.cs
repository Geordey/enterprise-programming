﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class BlogRepo : ConnectionClass
    {
        public BlogRepo() : base() { }

        public List<Common.blog> GetBlogs()
        {
            return Entity.blogs.toList();
        }
    }
}
