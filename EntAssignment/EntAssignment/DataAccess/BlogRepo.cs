﻿using EntAssignment.Common;
using EntAssignment.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class BlogRepo : ConnectionClass
    {
        public BlogRepo() : base() { }

        public List<blog> GetLatestBlogs(string category)
        {
            var cat = new CategoryRepo().GetCategory(category);
            return Entity.blogs.Where(x => x.categoryId == cat.Id).OrderByDescending(x => x.publishDate).Take(4).Skip(1).ToList();
        }

        public blog GetLatestBlog(string category)
        {
            var cat = new CategoryRepo().GetCategory(category);
            return Entity.blogs.OrderByDescending(x => x.publishDate).First(x => x.categoryId == cat.Id);
        }

        public List<blog>GetBlogsCarousel()
        {
            return Entity.blogs.OrderByDescending(x => x.publishDate).Take(5).ToList();
        }

 
        public string GetSummary(string text)
        {
            IEnumerable<string> words = text.Split().Take(30);
            return String.Join(" ", words);
           
        }

        public blog GetLatestByUser(string user) {
            return Entity.blogs.OrderByDescending(x => x.publishDate).First(x => x.authorId.Contains(user));
        }
        public List<blog> GetBlogsByUser(string user)
        {
            return Entity.blogs.Where(x => x.authorId.Contains(user)).OrderByDescending(x => x.publishDate).Skip(1).ToList();
        }

    }
}
