﻿using DataAccess;
using EntAssignment.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EntAssignment.DataAccess
{
    public class CategoryRepo : ConnectionClass
    {
        public CategoryRepo() : base(){}

        public category GetCategory(string catName)
        {
            return Entity.categories.Where(c => c.name == catName).FirstOrDefault();
        }

        public List<category> GetCategories()
        {
            return Entity.categories.ToList();
        }
    }
}