﻿using DataAccess;
using EntAssignment.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EntAssignment.DataAccess
{
    public class AuthorRepo : ConnectionClass
    {
        public AuthorRepo() : base() { }
   
        public author GetAuthor(string email)
        {
            return Entity.authors.FirstOrDefault(x => x.email.Contains(email));
        }
    }
}