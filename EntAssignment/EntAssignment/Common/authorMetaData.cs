﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EntAssignment.Common
{
    [MetadataType(typeof(authorMetaData))]
    public partial class author
    {

    }
    public class authorMetaData
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email Address")]
        public string email { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string firstName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string lastName { get; set; }
        [Required]
        [Display(Name = "Home Town")]
        public string homeTown { get; set; }
        [Required]
        [Display(Name = "Profession")]
        public string Profession { get; set; }
    }


}