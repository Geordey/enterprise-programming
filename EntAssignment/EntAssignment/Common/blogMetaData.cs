﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EntAssignment.Common
{
    [MetadataType(typeof(BlogMetaData))]
    public partial class blog
    {

    }

    public class BlogMetaData
    {
        [Required]
        [Display(Name = "Title")]
        public string heading { get; set; }
        [Required]
        [Display(Name = "Content")]
        public string text { get; set; }
        [Required]
        public Guid categoryId { get; set; }
        [Required]
        public string authorId { get; set; }
        [Required]
        public DateTime publishDate { get; set; }
        
        public string imagePath { get; set; }

        public string tags { get; set; }
    }
}