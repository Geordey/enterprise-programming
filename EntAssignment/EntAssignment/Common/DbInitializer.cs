﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EntAssignment.Common
{
    public class DbInitializer : DropCreateDatabaseIfModelChanges<codeMeEntities>
    {
        protected override void Seed(codeMeEntities context)
        {
            context.blogs.Add(new blog() { heading = "Direct From the seed",
                text = "Testing if the seed works",
                categoryId = new Guid("FFC21FAD-9A60-4D4A-B32F-1311F661A079"),
                authorId = "geordeylgatt@gmail.com",
                publishDate = DateTime.Now,
                imagePath = "\\images\\0xgnFC.jpg"
            });
        }
    }
}