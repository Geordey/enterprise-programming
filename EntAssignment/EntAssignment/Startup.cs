﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EntAssignment.Startup))]
namespace EntAssignment
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
