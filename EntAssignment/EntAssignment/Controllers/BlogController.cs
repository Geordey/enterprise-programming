﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BusinessLogic;
using EntAssignment.BusinessLogic;
using EntAssignment.Common;
using EntAssignment.DataAccess;
using EntAssignment.Models;

namespace EntAssignment.Controllers
{
    public class BlogController : Controller
    {
        private codeMeEntities db = new codeMeEntities();

        // GET: Blog
        //public ActionResult Index()
        //{
        //    return View(db.blogs.Where(x=>x.authorId == User.Identity.Name).ToList());
        //}

        // GET: Blog/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            blog blog = db.blogs.Find(id);
            category c = db.categories.Find(blog.categoryId);

            if (blog == null)
            {
                return HttpNotFound();
            }

            blogViewModel bg = new blogViewModel();
            bg.b = blog;
            bg.c = c;

            return View(bg);
        }
        
        // GET: Blog/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Blog/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(blog blog,HttpPostedFileBase file)
        {
            string ImageName = System.IO.Path.GetFileName(file.FileName);
            string physicalPath = Server.MapPath("~/images/" + ImageName);


            AuthorController ac = new AuthorController();
            AuthorRepo ar = new AuthorRepo();
            if(ar.GetAuthor(User.Identity.Name) == null)
            {
                author a = new author();
                a.email = User.Identity.Name;
                ac.Create(a);
            }


            string path = "\\images\\";
            file.SaveAs(physicalPath);
            // save image in folder

            blog.Id = Guid.NewGuid();
            blog.publishDate = DateTime.Now;
            blog.authorId = User.Identity.Name;
            if(blog.tags == null)
            {
                blog.tags = "";
            }
            blog.imagePath = path + ImageName;

            if (ModelState.IsValid)
                {
                    db.blogs.Add(blog);
                    db.SaveChanges();
                    return RedirectToAction("Index","Home");
                }

            return RedirectToAction("Index","Home");
        }

        // GET: Blog/Edit/5
        [Authorize]
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            blog blog = db.blogs.Find(id);
            if (blog == null)
            {
                return HttpNotFound();
            }
            return View(blog);
        }

        // POST: Blog/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(blog blog)
        {
            var newContext = new codeMeEntities();

            blog b = db.blogs.Find(blog.Id);
            blog.imagePath = b.imagePath;
            blog.publishDate = b.publishDate;
            
            if (blog.tags == null)
            {
                blog.tags=b.tags;
            }

            if (ModelState.IsValid)
            {
                newContext.Entry(blog).State = EntityState.Modified;
                newContext.SaveChanges();
                return RedirectToAction("Index","Home");
            }
            return View(blog);
        }

        // GET: Blog/Delete/5
        [Authorize]
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            blog blog = db.blogs.Find(id);
            if (blog == null)
            {
                return HttpNotFound();
            }
            return View(blog);
        }

        // POST: Blog/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            blog blog = db.blogs.Find(id);
            db.blogs.Remove(blog);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
