﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Upload;
using Google.Apis.Util.Store;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;
using System.Threading;
using System.Reflection;
using EntAssignment.Models;

namespace RealEstateGitHub.Controllers
{
    public class ExternalDataController : Controller
    {
        // GET: Video
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            VideoModel model = new VideoModel()
            {
                Videos = await GetVideos()
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<ActionResult> Index(HttpPostedFileBase file)
        {
            var stream = file.InputStream;

            await BeginUpload(stream);
            return RedirectToAction("index", "ExternalData");
        }

        private async Task BeginUpload(Stream stream, String videoTitle = "Title", String videoDescription = "Description", string[] tags = null, string categoryId = "22", string privacyStatus = "unlisted")
        {
            if (tags == null)
            {
                tags = new string[] { "sampleTag1", "sampleTag2" };
            }

            UserCredential credential;
            using (var credentialStream = new FileStream(Server.MapPath("~/App_Data/client_secret.json"), FileMode.Open, FileAccess.Read))
            {
                credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(credentialStream).Secrets,
                    new[] { YouTubeService.Scope.YoutubeUpload },
                    "user",
                    CancellationToken.None
                );
            }

            var youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
            });

            var video = new Video();
            video.Snippet = new VideoSnippet();
            video.Snippet.Title = videoTitle;
            video.Snippet.Description = videoDescription;
            video.Snippet.Tags = tags;
            video.Snippet.CategoryId = categoryId;
            video.Status = new VideoStatus();
            video.Status.PrivacyStatus = privacyStatus;

            var videosInsertRequest = youtubeService.Videos.Insert(video, "snippet,status", stream, "video/*");

            videosInsertRequest.ProgressChanged += videosInsertRequest_ProgressChanged;
            videosInsertRequest.ResponseReceived += videosInsertRequest_ResponseReceived;

            await videosInsertRequest.UploadAsync();

        }

        void videosInsertRequest_ProgressChanged(Google.Apis.Upload.IUploadProgress progress)
        {
            switch (progress.Status)
            {
                case UploadStatus.Uploading:
                    System.Diagnostics.Debug.WriteLine("{0} bytes sent.", progress.BytesSent);
                    break;

                case UploadStatus.Failed:
                    System.Diagnostics.Debug.WriteLine("An error prevented the upload from completing.\n{0}", progress.Exception);
                    break;
            }
        }

        void videosInsertRequest_ResponseReceived(Video video)
        {
            System.Diagnostics.Debug.WriteLine("Video " + video.Id + " was successfully uploaded.");
        }

        public async Task<List<String>> GetVideos()
        {
            UserCredential credential;
            using (var stream = new FileStream(Server.MapPath("~/App_Data/client_secret.json"), FileMode.Open, FileAccess.Read))
            {
                credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    new[] { YouTubeService.Scope.YoutubeReadonly },
                    "user",
                    CancellationToken.None,
                    new FileDataStore(this.GetType().ToString())
                );
            }

            var youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = this.GetType().ToString()
            });

            var channelsListRequest = youtubeService.Channels.List("contentDetails");
            channelsListRequest.Mine = true;

            // Retrieve the contentDetails part of the channel resource for the authenticated user's channel.
            var channelsListResponse = await channelsListRequest.ExecuteAsync();

            foreach (var channel in channelsListResponse.Items)
            {
                // From the API response, extract the playlist ID that identifies the list
                // of videos uploaded to the authenticated user's channel.
                var uploadsListId = channel.ContentDetails.RelatedPlaylists.Uploads;

                System.Diagnostics.Debug.WriteLine("Videos in list {0}", uploadsListId);

                var nextPageToken = "";
                while (nextPageToken != null)
                {
                    var playlistItemsListRequest = youtubeService.PlaylistItems.List("snippet");
                    playlistItemsListRequest.PlaylistId = uploadsListId;
                    playlistItemsListRequest.MaxResults = 50;
                    playlistItemsListRequest.PageToken = nextPageToken;

                    // Retrieve the list of videos uploaded to the authenticated user's channel.
                    var playlistItemsListResponse = await playlistItemsListRequest.ExecuteAsync();

                    List<string> vids = new List<string>();
                    foreach (var playlistItem in playlistItemsListResponse.Items)
                    {
                        // Print information about each video.
                        vids.Add(playlistItem.Snippet.Title + ":" + playlistItem.Snippet.ResourceId.VideoId);
                        System.Diagnostics.Debug.WriteLine("{0} ", playlistItem.Snippet.Title);
                    }

                    nextPageToken = playlistItemsListResponse.NextPageToken;

                    return vids;
                }

            }
            return null;
        }
    }
}