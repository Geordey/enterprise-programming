﻿using EntAssignment.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EntAssignment.Models
{
    public class CategoryViewModel
    {
        public category c {get;set;}
        public blog fb { get; set; }
        public List<blog> b {get; set;}

        public CategoryViewModel()
        {
            this.c = new category();
            this.fb = new blog();
            this.b = new List<blog>();
        }
    }
}