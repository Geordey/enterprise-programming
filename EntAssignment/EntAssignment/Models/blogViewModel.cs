﻿using EntAssignment.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EntAssignment.Models
{
    public class blogViewModel
    {

        public blog b { get; set; }
        public category c { get; set; }

        public blogViewModel()
        {
            this.b = new blog();
            this.c = new category();
        }
    }
}