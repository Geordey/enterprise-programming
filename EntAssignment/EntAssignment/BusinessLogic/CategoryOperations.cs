﻿using EntAssignment.Common;
using EntAssignment.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EntAssignment.BusinessLogic
{
    public class CategoryOperations
    {
        CategoryRepo Cr = new CategoryRepo();

        public List<category> GetCategories()
        {
            return Cr.GetCategories();
        }


        public category GetCategory(string catName)
        {
            return Cr.GetCategory(catName);
        }
    }
}