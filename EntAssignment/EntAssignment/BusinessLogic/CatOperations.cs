﻿using EntAssignment;
using EntAssignment.Common;
using EntAssignment.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntAssignment.BusinessLogic
{
    public class CatOperations
    {
         CategoryRepo cr = new CategoryRepo();

        public category GetCategory(string name)
        {
            return cr.GetCategory(name);
        }

        public List<category> GetCategories()
        {
            return cr.GetCategories();
        }
    }
}