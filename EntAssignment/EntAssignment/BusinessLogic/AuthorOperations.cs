﻿using EntAssignment.Common;
using EntAssignment.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EntAssignment.BusinessLogic
{
    public class AuthorOperations
    {

        public AuthorRepo ar = new AuthorRepo();

        public author GetAuthor (string email)
        {
           return ar.GetAuthor(email);
        }
    }
}