﻿using DataAccess;
using EntAssignment;
using EntAssignment.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class BlogOperations
    {
        BlogRepo br = new BlogRepo();

        public List<blog> GetLatestBlogs(string cat)
        {
            return br.GetLatestBlogs(cat);
        }   


        public blog GetLatestBlog(string cat)
        {
            return br.GetLatestBlog(cat);
        }

        public List<blog> GetBlogsCarousel()
        {
            return br.GetBlogsCarousel();
        }

        public string GetSummary(string text)
        {
            return br.GetSummary(text);
        }

        public List<blog> GetBlogsByUser(string user)
        {
            return br.GetBlogsByUser(user);
        }

        public blog GetLatestByUser(string user)
        {
            return br.GetLatestByUser(user);
        }
    }
}
